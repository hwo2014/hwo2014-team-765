import json
import socket
import sys


class HamerBot(object):

    def __init__(self, socket, name = "Hamer", key = "GNIQ2WFdKcGW4A"):
        self.socket = socket
        self.name = name
        self.key = key
        self.pieces = []
        self.angles = []
        self.switches = {}
        self.switched = False
        self.message = None
        self.thr = 1.
        self.last_piece = 0
        self.last_pos = 0
        self.speed = 0.
        self.max_speed = 10.

        print "Car " + self.name + " ready"

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def send_msg(self):
        if self.message:
            msg_type, data = self.message
            self.msg(msg_type, data)
            self.message = None
        else:
            self.throttle(self.thr)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_init(self, data):
        self.pieces = data["race"]["track"]["pieces"]
        n = len(self.pieces)
        last_switch = 0
        last_angle = False
        turn = 0
        angle = 0.
        angle_num = 0
        for k in range(0,n):
            if "switch" in self.pieces[k] and last_switch != k:
                if abs(turn) > 45:
                    if turn > 0:
                        self.switches[last_switch-1] = "Right"
                    else:
                        self.switches[last_switch-1] = "Left"
                last_switch = k
                turn = 0
            if "angle" in self.pieces[k]:
                turn += self.pieces[k]["angle"]
                angle += abs(float(self.pieces[k]["radius"])/float(self.pieces[k]["angle"]))
                angle_num += 1
                last_angle = True
            elif last_angle:
                start = k-angle_num
                if start < 0:
                    start += n
                self.angles += [(k-angle_num,k,angle)]
                angle = 0.
                angle_num = 0
                last_angle = False
        

    def on_game_start(self, data):
        print("Race started")
        self.throttle(1)

    def angle_speed(self, fact):
        if fact <= 10:
            return 4.5
        if fact <= 30:
            return 5.5
        if fact <= 42:
            return 6.5
        else:
            return 10

    def on_car_positions(self, data):
        piece = data[0]["piecePosition"]["pieceIndex"]
        pos = data[0]["piecePosition"]["inPieceDistance"]

        if ((piece != self.last_piece)) or (piece == 0 and pos > 50):
            if piece in self.switches and not(self.switched):
                self.message = ("switchLane",self.switches[piece])
                self.switched = True
            else:
                self.switched = False

        if piece == self.last_piece:
            self.speed = pos - self.last_pos
        else:
            self.last_piece = piece

        self.last_pos = pos

        self.thr = -1
        max_speed = self.max_speed
        for angle in self.angles:
            start, end, fact = angle
            if start <= piece+1 and end > piece:
                max_speed = self.angle_speed(fact)
        if piece+1 < len(self.pieces) and ("angle" in self.pieces[piece+1] or "angle" in self.pieces[piece]) and self.speed > max_speed:
            self.thr = 0
        else:
            self.thr = 1

        self.send_msg()

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        k = 0
        while line:
            k += 1
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = HamerBot(s, name, key)
        bot.run()
